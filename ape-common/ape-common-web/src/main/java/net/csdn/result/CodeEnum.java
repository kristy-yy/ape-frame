package net.csdn.result;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * @author yuyang
 */

@Getter
@AllArgsConstructor
public enum CodeEnum {

    SUCCESS(200, "请求成功"),
    FAILURE(400, "请求失败"),
    SIGN_INVALID(511,"sign invalid"),
    NOT_FOUND(404,"未找到"),
    HEALTH_BAD(500, "fail"),
    ;
    /**
     * 错误代码
     */
    private Integer code;

    /**
     * 错误描述
     */
    private String message;

}
