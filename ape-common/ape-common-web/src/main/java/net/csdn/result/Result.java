package net.csdn.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yuyang
 */
@Data
public class Result<T> implements Serializable {

    private static final long serialVersionUID = -86270333671399977L;
    /**
     * 返回码
     */
    private Boolean success;
    /**
     * 返回码
     */
    private Integer code;

    /**
     * 提示信息
     */
    private String message;

    /**
     * 返回数据
     */
    private T data;


    public static <T> Result<T> success() {
        Result<T> result = new Result<T>();
        result.setSuccess(true);
        result.setCode(CodeEnum.SUCCESS.getCode());
        result.setMessage(CodeEnum.SUCCESS.getMessage());
        return result;

    }

    public static <T> Result<T> success(T data) {
        Result<T> result = new Result<T>();
        result.setSuccess(true);
        result.setCode(CodeEnum.SUCCESS.getCode());
        result.setMessage(CodeEnum.SUCCESS.getMessage());
        result.setData(data);
        return result;

    }

    public static <T> Result<T> success(CodeEnum codeEnum) {
        Result<T> result = new Result<T>();
        result.setSuccess(true);
        result.setCode(codeEnum.getCode());
        result.setMessage(codeEnum.getMessage());
        return result;
    }

    public static <T> Result<T> success(CodeEnum codeEnum, T data) {
        Result<T> result = new Result<T>();
        result.setSuccess(true);
        result.setCode(codeEnum.getCode());
        result.setMessage(codeEnum.getMessage());
        result.setData(data);
        return result;
    }

    public static <T> Result<T> success(CodeEnum codeEnum, String message) {
        Result<T> result = new Result<T>();
        result.setSuccess(false);
        result.setCode(codeEnum.getCode());
        result.setMessage(message);
        return result;
    }

    public static <T> Result<T> failure() {
        Result<T> result = new Result<T>();
        result.setSuccess(true);
        result.setCode(CodeEnum.FAILURE.getCode());
        result.setMessage(CodeEnum.FAILURE.getMessage());
        return result;

    }

    public static <T> Result<T> failure(CodeEnum codeEnum) {
        Result<T> result = new Result<T>();
        result.setSuccess(false);
        result.setCode(codeEnum.getCode());
        result.setMessage(codeEnum.getMessage());
        return result;
    }

    public static <T> Result<T> failure(CodeEnum codeEnum, T data) {
        Result<T> result = new Result<T>();
        result.setSuccess(false);
        result.setCode(codeEnum.getCode());
        result.setMessage(codeEnum.getMessage());
        result.setData(data);
        return result;
    }

    public static <T> Result<T> failure(CodeEnum codeEnum, String message) {
        Result<T> result = new Result<T>();
        result.setSuccess(false);
        result.setCode(codeEnum.getCode());
        result.setMessage(message);
        return result;
    }

}
