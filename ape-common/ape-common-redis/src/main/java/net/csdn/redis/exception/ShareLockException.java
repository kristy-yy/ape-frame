package net.csdn.redis.exception;

/**
 * @author yuyang
 */
public class ShareLockException extends RuntimeException{

    public ShareLockException(String message){
        super(message);
    }

}
