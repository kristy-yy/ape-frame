package net.csdn.user;


import net.csdn.user.entity.User;
import net.csdn.user.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.aop.support.AopUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@SpringBootTest(classes = UserApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Resource
    private UserService userService;

    @Test
    public void testQuery() {
        User user = userService.queryById(1L);
        System.out.println(user);
    }

    @Test
    public void testAopUtils() {
        Class<?> targetClass = AopUtils.getTargetClass(userService);
        System.out.println(targetClass);
    }




}
