package net.csdn.user.init;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApplicationInit implements ApplicationListener<ApplicationReadyEvent> {

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        try {
            long start = System.currentTimeMillis();
            initFastJson();
            log.info("ApplicationInit{}.costTime{}", "预热fastjson", System.currentTimeMillis() - start);
        } catch (Exception e) {
            log.error("ApplicationInit{}.error", "预热fastjson", e);
        }
    }

    private void initFastJson() {
        System.out.println("预热fastjson");
    }

}
