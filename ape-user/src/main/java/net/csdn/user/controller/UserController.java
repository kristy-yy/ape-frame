package net.csdn.user.controller;

import lombok.extern.slf4j.Slf4j;
import net.csdn.result.PageResponse;
import net.csdn.result.Result;
import net.csdn.user.entity.User;
import net.csdn.user.req.UserPageReq;
import net.csdn.user.service.UserExcelExport;
import net.csdn.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 兑换码列表(User)表控制层
 *
 * @author makejava
 * @since 2023-09-26 12:25:43
 */
@RestController
@RequestMapping("user")
@Slf4j
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    @Autowired
    private UserExcelExport userExcelExport;

    /**
     * 分页查询
     *
     * @param userPageReq      分页对象
     * @return 查询结果
     */
    @GetMapping
    public Result<PageResponse<User>> queryByPage(UserPageReq userPageReq) {
        log.info("11111111111111111");
        return Result.success(this.userService.queryByPage(userPageReq));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    //@Cacheable(cacheNames = "user",key = "'queryById'+#id")
    public Result<User> queryById(@PathVariable("id") Long id) {
        return Result.success(this.userService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param user 实体
     * @return 新增结果
     */
    @PostMapping
    public Result<User> add(@RequestBody User user) {
        return Result.success(this.userService.insert(user));
    }

    /**
     * 编辑数据
     *
     * @param user 实体
     * @return 编辑结果
     */
    @PutMapping
    public Result<User> edit(@RequestBody User user) {
        return Result.success(this.userService.update(user));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public Result<Boolean> deleteById(Long id) {
        return Result.success(this.userService.deleteById(id));
    }


    /**
     * 导出excel
     *
     */
    @GetMapping("export")
    public void export() {
        //指定数据条件
        Map<String, Object> map = new HashMap<>();
        userExcelExport.exportWithBigData("用户列表", map);
    }

}

