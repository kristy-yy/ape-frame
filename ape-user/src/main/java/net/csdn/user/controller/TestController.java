package net.csdn.user.controller;

import net.csdn.redis.service.RedisService;
import net.csdn.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yuyang
 */
@RestController
public class TestController {

    @Autowired
    private RedisService redisService;

    @GetMapping("/test")
    public Result test(){
        redisService.set("abcabc","111111");
        String a = (String) redisService.get("abcabc");
        return Result.success();
    }
}
