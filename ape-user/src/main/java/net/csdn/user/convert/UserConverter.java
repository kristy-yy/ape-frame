package net.csdn.user.convert;


import net.csdn.user.entity.User;
import net.csdn.user.req.UserPageReq;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author yuyang
 */
@Mapper
public interface UserConverter {

    UserConverter INSTANCE = Mappers.getMapper(UserConverter.class);

    User convertReqToUser(UserPageReq userPageReq);

}
