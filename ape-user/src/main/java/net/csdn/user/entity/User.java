package net.csdn.user.entity;

import lombok.Data;
import net.csdn.entity.BaseEntity;

import java.util.Date;
import java.io.Serializable;

/**
 * 兑换码列表(User)实体类
 *
 * @author makejava
 * @since 2023-09-26 12:25:44
 */
@Data
public class User extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -25004366446054190L;
    /**
     * 自增长ID
     */
    private Long id;
    /**
     * 订单号码
     */
    private String name;
    /**
     * 商品id
     */
    private Integer age;


}

