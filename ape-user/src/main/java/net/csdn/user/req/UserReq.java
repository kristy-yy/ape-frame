package net.csdn.user.req;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import net.csdn.entity.BaseEntity;

/**
 * @author yuyang
 */
@Data
public class UserReq extends BaseEntity {

    private Long id;

    private String name;

    private Integer age;


}
