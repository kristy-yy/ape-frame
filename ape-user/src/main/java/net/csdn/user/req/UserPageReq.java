package net.csdn.user.req;

import lombok.Data;
import net.csdn.entity.BaseEntity;
import net.csdn.result.PageRequest;

import java.io.Serializable;

/**
 * @author yuyang
 */
@Data
public class UserPageReq extends PageRequest implements Serializable {

    private Long id;

    private String name;

    private Integer age;

}
