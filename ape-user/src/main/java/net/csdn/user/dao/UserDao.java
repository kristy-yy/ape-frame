package net.csdn.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.csdn.user.entity.User;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * 兑换码列表(User)表数据库访问层
 *
 * @author makejava
 * @since 2023-09-26 12:25:43
 */
public interface UserDao extends BaseMapper<User>, Serializable {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    User queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param user 查询条件
     * @param pageStart 开始
     * @param pageSize         分页对象
     * @return 对象列表
     */
    List<User> queryAllByLimit(@Param("po") User user, @Param("pageStart") Long pageStart,@Param("pageSize") Long pageSize);

    /**
     * 统计总行数
     *
     * @param user 查询条件
     * @return 总行数
     */
    long count(User user);

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int insert(User user);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<User> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<User> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<User> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<User> entities);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int update(User user);


}

