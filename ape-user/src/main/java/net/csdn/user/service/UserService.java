package net.csdn.user.service;

import net.csdn.result.PageResponse;
import net.csdn.user.entity.User;
import net.csdn.user.req.UserPageReq;

import java.util.Map;

/**
 * 兑换码列表(User)表服务接口
 *
 * @author makejava
 * @since 2023-09-26 12:25:44
 */
public interface UserService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    User queryById(Long id);

    /**
     * 分页查询
     *
     * @param userPageReq      分页对象
     * @return 查询结果
     */
    PageResponse<User> queryByPage(UserPageReq userPageReq);

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    User insert(User user);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    User update(User user);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

    Long queryCount(Map<String, Object> conditions);

}
