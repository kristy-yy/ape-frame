package net.csdn.user.service.impl;

import lombok.extern.slf4j.Slf4j;
import net.csdn.result.PageResponse;
import net.csdn.user.convert.UserConverter;
import net.csdn.user.entity.User;
import net.csdn.user.dao.UserDao;
import net.csdn.user.req.UserPageReq;
import net.csdn.user.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 兑换码列表(User)表服务实现类
 *
 * @author makejava
 * @since 2023-09-26 12:25:44
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public User queryById(Long id) {
        return this.userDao.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param userPageReq      分页对象
     * @return 查询结果
     */
    @Override
    public PageResponse<User> queryByPage(UserPageReq userPageReq) {
        PageResponse<User> pageResponse = new PageResponse<>();
        User user = UserConverter.INSTANCE.convertReqToUser(userPageReq);
        pageResponse.setCurrent(userPageReq.getPageNo());
        pageResponse.setPageSize(userPageReq.getPageSize());
        Long pageStart = (userPageReq.getPageNo() - 1) * userPageReq.getPageSize();
        long total = this.userDao.count(user);
        List<User> userList = this.userDao.queryAllByLimit(user, pageStart, userPageReq.getPageSize());
        pageResponse.setTotal(total);
        pageResponse.setRecords(userList);
        return pageResponse;
    }

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public User insert(User user) {
        this.userDao.insert(user);
        return user;
    }

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public User update(User user) {
        this.userDao.update(user);
        return this.queryById(user.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.userDao.deleteById(id) > 0;
    }

    @Override
    public Long queryCount(Map<String, Object> conditions) {
        User user = new User();
        return userDao.count(user);
    }
}
