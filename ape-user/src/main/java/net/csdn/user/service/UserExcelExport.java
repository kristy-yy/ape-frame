package net.csdn.user.service;

import net.csdn.excel.BaseEasyExcelExport;
import net.csdn.result.PageResponse;
import net.csdn.user.entity.User;
import net.csdn.user.req.UserPageReq;
import net.csdn.user.req.UserReq;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;


/**
 * @author yuyang
 */
@Component
public class UserExcelExport extends BaseEasyExcelExport<User> {

    @Resource
    private UserService userService;

    /**
     * 数据导出
     */
    public void exportWithBigData(String fileName, Map<String, Object> conditions) {
        this.exportExcel(fileName, conditions);
    }

    @Override
    protected List<List<String>> getExcelHead() {
        List<List<String>> head = new ArrayList<>();
        head.add(Collections.singletonList("用户编号"));
        head.add(Collections.singletonList("用户姓名"));
        return head;
    }

    @Override
    protected Long dataTotalCount(Map<String, Object> conditions) {
        return userService.queryCount(conditions);
    }

    @Override
    protected Long eachSheetTotalCount() {
        return 5000L;
    }

    @Override
    protected Long eachTimesWriteSheetTotalCount() {
        return 2000L;
    }

    @Override
    protected void buildDataList(List<List<String>> resultList, Map<String, Object> condition,
                                 Long pageNo, Long pageSize) {
        UserPageReq userPageReq = new UserPageReq();
        //可以根据condition设置条件
        userPageReq.setPageNo(pageNo);
        userPageReq.setPageSize(pageSize);
        PageResponse<User> pageResponse = userService.queryByPage(userPageReq);
        List<User> userList = pageResponse.getResult();
        if (CollectionUtils.isNotEmpty(userList)) {
            userList.forEach(user -> {
                resultList.add(Arrays.asList(user.getId().toString(), user.getName()));
            });
        }
    }

}
